const path = require('path');
const HTMLWebpackPlugin = require('html-webpack-plugin');

// For more info on webpack 4 configuration, see:
// https://www.valentinog.com/blog/webpack-4-tutorial/
module.exports = {
	entry: './src/app.js',
	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: '[name].[hash].js',
	},
	devServer: {
		port: 3000,
		hot: true,
		overlay: true,
		noInfo: true,
		historyApiFallback: true,
		clientLogLevel: 'warning',
	},
	devtool: 'cheap-module-source-map',
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: [
					'babel-loader'
				],
			}, {
				test: /\.css$/,
				use: [ 'style-loader', 'css-loader' ],
			}, {
				test: /\.scss$/,
				use: [ 'style-loader', 'css-loader', 'sass-loader' ],
			}, {
				test: /\.(jpeg|jpg|png|gif|svg|woff|woff2|ttf|otf|eot)$/,
				use: [ 'file-loader' ],
			},
		],
	},
	plugins: [
		new HTMLWebpackPlugin({
			template: 'src/index.ejs',
			favicon: 'src/favicon.ico',
		}),
	],
};
