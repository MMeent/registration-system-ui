import React from "react";

import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardHeader from "@material-ui/core/CardHeader";
import CheckBox from "@material-ui/core/CheckBox";

import Divider from "@material-ui/core/Divider";
import FormControl from "@material-ui/core/FormControl";
import Grid from "@material-ui/core/Grid";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import Paper from "@material-ui/core/Paper";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";

import withStyles from "@material-ui/core/styles/withStyles";

import { styles } from '../../../../style/styles';


const dayCost = ({ cost, extras }) => (cost + extras.reduce((tot, opt) => tot + opt.cost, 0));

class CompleteOrder extends React.Component {
	handleBookableAction(bookable) {
		return (evt) => {
			evt.preventDefault();
			const currentItems = this.props.order.items;
			if (currentItems.some(item => item.name === bookable.name)) {
				this.props.setOrderField("items", currentItems.filter(item => item.name !== bookable.name))
			} else {
				this.props.setOrderField(
					"items",
					currentItems.concat([{
						name: bookable.name,
						cost: bookable.cost,
						extras: [],
					}])
				);
			}
		}
	}

	handleOnClickExtra(bookable, extra) {
		return (evt) => {
			evt.preventDefault();
			this.props.setOrderField(
				"items",
				this.props.order.items.map(item => {
					if (item.name !== bookable.name) {
						return item;
					}

					if (item.extras.some(ex => ex.name === extra.name)) {
						return {
							...item,
							extras: item.extras.filter(ex => ex.name !== extra.name),
						};
					} else {
						return {
							...item,
							extras: item.extras.concat([{ ...extra }]),
						}
					}
				})
			)
		}
	}

	render() {
		const { classes, order } = this.props;

		return (
			<>
				<Typography variant="h6" gutterBottom>
					Ticket information
				</Typography>
				<Grid container spacing={40}>
					{
						bookables.map(bookable => {
							const booked = order.items.find(it => it.name === bookable.name);
							const hasBooked = !!booked;

							return (
								<Grid key={bookable.name} item xs={12} md={4}>
									<Card>
										<CardHeader title={bookable.name} subheader={`€\u00A0${(booked ? dayCost(booked) : 0).toFixed(2)}`} className={classes.cardHeader} />
										<CardContent>
											<List>
												<ListItem button onClick={this.handleBookableAction(bookable)}>
													<ListItemText primary="Ticket" secondary={`€ ${bookable.cost.toFixed(2)}`}/>
													<ListItemSecondaryAction>
														<CheckBox checked={hasBooked} onClick={this.handleBookableAction(bookable)} />
													</ListItemSecondaryAction>
												</ListItem>
												{
													bookable.extras.map(extra => {
														const hasBookedExtra = !!(booked && booked.extras.some(ex => ex.name === extra.name));
														console.log(booked, bookable, extra, hasBookedExtra);
														return (
															<ListItem key={extra.name} button disabled={!hasBooked} onClick={this.handleOnClickExtra(bookable, extra)}>
																<ListItemText inset primary={extra.name} secondary={`€ ${extra.cost.toFixed(2)}`}/>
																<ListItemSecondaryAction>
																	<CheckBox disabled={!hasBooked} checked={!!hasBookedExtra} onClick={this.handleOnClickExtra(bookable, extra)} />
																</ListItemSecondaryAction>
															</ListItem>
														);
													})
												}
											</List>
										</CardContent>
									</Card>
								</Grid>
							);
						})
					}
				</Grid>
			</>
		);
	}
}

export default withStyles(styles)(CompleteOrder)
