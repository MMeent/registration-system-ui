import React from "react";

import Divider from "@material-ui/core/Divider";
import FormControl from "@material-ui/core/FormControl";
import Grid from "@material-ui/core/Grid";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Paper from "@material-ui/core/Paper";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";

import withStyles from "@material-ui/core/styles/withStyles";

import ExpandLessIcon from "@material-ui/icons/ExpandLess";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";

import { styles } from '../../../../style/styles';

const dayCost = ({ cost, extras }) => (cost + extras.reduce((tot, opt) => tot + opt.cost, 0));
const fmtOption = ({ name, cost }) => `${name} (€ ${cost.toFixed(2)})`;


class StageThree extends React.Component {
	render() {
		const { order, classes } = this.props;

		return (
			<>
				<Typography variant="h6" gutterBottom>
					Confirm order
				</Typography>
				<List>
					<ListItem>
						<ListItemText primary={ order.name } secondary={
							`Totalling € ${(order.items.reduce((sum, item) => (sum + dayCost(item)), 0) + order.extras.reduce((sum, extra) => (sum + extra.cost), 0)).toFixed(2)}`
						} />
						<Typography variant="subtitle1" className={classes.total}>
						</Typography>
					</ListItem>
					<List component="div" disablePadding>
						{
							order.items.map(item => (
								<ListItem className={classes.nested}>
									<ListItemText
										inset
										primary={item.name}
										secondary={`Ticket (€ ${item.cost.toFixed(2)})` + (item.extras.reduce(
											(result, el) => (result === null)
												? ` + ${fmtOption(el)}`
												: `${result}, ${fmtOption(el)}`,
											null
										) || '')}
									/>
									<Typography variant="subtitle1" className={classes.total}>
										€ {dayCost(item).toFixed(2)}
									</Typography>
								</ListItem>
							))
						}
					</List>
			</List>
			</>
		);
	}
}

export default withStyles(styles)(StageThree)
