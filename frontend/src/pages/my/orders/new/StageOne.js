import React from "react";

import Divider from "@material-ui/core/Divider";
import FormControl from "@material-ui/core/FormControl";
import Grid from "@material-ui/core/Grid";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Paper from "@material-ui/core/Paper";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";

import withStyles from "@material-ui/core/styles/withStyles";

import { styles } from '../../../../style/styles';



class StageOne extends React.Component {
	setName = ({ target: { value }}) => this.props.setOrderField("name", value);
	setDOB = ({ target: { value }}) => this.props.setOrderField("dateOfBirth", value);

	render() {
		const { classes, order } = this.props;

		return (
			<>
				<Typography variant="h6" gutterBottom>
					Attendee information
				</Typography>
				<Grid container spacing={32}>
					<Grid item xs={12}>
						<TextField
							required
							id="name"
							name="name"
							label="Name"
							fullWidth
							autoComplete="name"
							value={order.name}
							onChange={this.setName}
						/>
					</Grid>
					<Grid item xs={12}>
						<FormControl required fullWidth>
							<InputLabel htmlFor="dob">Date of birth</InputLabel>
							<Input name="dob" type="date" id="dob" autoComplete="dob" value={order.dateOfBirth} onChange={this.setDOB} />
						</FormControl>
					</Grid>
				</Grid>
			</>
		);
	}
}

export default withStyles(styles)(StageOne)
