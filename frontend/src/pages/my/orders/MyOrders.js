import React from "react";

import Divider from "@material-ui/core/Divider";
import Grid from "@material-ui/core/Grid";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import withStyles from "@material-ui/core/styles/withStyles";
import { styles } from '../../../style/styles';
import OrderList_Item from '../../../elements/order/OrderList_Item';


const dinner = {
	name: "dinner",
	cost: 18.50,
};

const sushi = {
	name: "sushi-workshop",
	cost: 20.00,
};


const orders = [
	{
		name: "Jan Peters",
		items: [
			{ name: "Friday", extras: [], cost: 30.00 },
			{ name: "Saturday", extras: [], cost: 30.00 },
			{ name: "Sunday", extras: [], cost: 30.00 },
		],
		extras: [],
	},
	{
		name: "Jantje Peters",
		items: [
			{ name: "Friday", extras: [], cost: 30.00 },
			{ name: "Saturday", extras: [ dinner ], cost: 30.00 },
		],
		extras: [],
	},
	{
		name: "Janus Petersz",
		items: [
			{ name: "Saturday", extras: [], cost: 30.00 },
			{ name: "Sunday", extras: [ dinner, sushi ], cost: 30.00 },
		],
		extras: [],
	},
];


class MyOrders extends React.Component {
	render() {
		const { classes } = this.props;

		return (
			<Grid container spacing={32} direction="column" alignItems="stretch" >
				<Grid item xs={12}>
					<Typography variant="h5">My orders</Typography>
				</Grid>
				<Grid item xs={12}>
					<Paper>
					<List>
						<ListItem>
							<ListItemText primary="Abunai 2018" />
						</ListItem>
						<Divider />
						{
							orders.map(order => (
								<OrderList_Item order={order} />
							))
						}
					</List>
					</Paper>
				</Grid>
				<Grid item>
					<Paper>
					<List>
						<ListItem>
							<ListItemText primary="Abunai 2017" />
						</ListItem>
						<Divider />
						{
							orders.map(order => (
								<OrderList_Item order={order} />
							))
						}
					</List>
					</Paper>
				</Grid>
			</Grid>
		);
	}
}

export default withStyles(styles)(MyOrders)
