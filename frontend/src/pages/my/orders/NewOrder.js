import React from "react";

import Button from "@material-ui/core/Button";
import Divider from "@material-ui/core/Divider";
import Grid from "@material-ui/core/Grid";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Paper from "@material-ui/core/Paper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import Stepper from "@material-ui/core/Stepper";
import Typography from "@material-ui/core/Typography";
import withStyles from "@material-ui/core/styles/withStyles";

import { styles } from '../../../style/styles';
import StageOne from './new/StageOne';
import StageTwo from './new/StageTwo';
import StageThree from './new/StageThree';


const days = [
	{
		name: "Friday",
		extras: [
			{
				name: "Dinner",
				cost: 30.00,
			},
		],
		cost: 30.00
	},
	{
		name: "Saturday",
		extras: [
			{
				name: "Dinner",
				cost: 30.00,
			},
			{
				name: "Sushi workshop",
				cost: 30.00,
			},
		],
		cost: 30.00
	},
	{
		name: "Sunday",
		extras: [
			{
				name: "Dinner",
				cost: 30.00,
			},
			{
				name: "Sushi workshop",
				cost: 30.00,
			},
		],
		cost: 30.00
	},
];


const steps = [ "Attendee information", "Ticket details", "Confirm order" ];
const stepComponents = [ StageOne, StageTwo, StageThree, StageOne ];

class NewOrder extends React.Component {
	state = {
		order: {
			name: "",
			items: [],
			extras: [],
		},
		activeStep: 0,
	};

	setName = ({ target: { value } }) => this.setState(({ order }) => ({ order: { ...order, name: value } }));
	addItem = (item) => this.setState(({ order }) => ({ order: { ...order, items: order.items.concat([item]) } }));
	removeItem = (item) => this.setState(({ order }) => ({ order: { ...order, items: order.items.filter(el => el !== item ) } }));


	setOrderField = (field, value) => this.setState(({ order }) => ({ order: { ...order, [field]: value } }));

	resetOrder = () => this.setState(() => ({ order: { name: "", items: [] } }));

	handleNext = () => this.setState(({ activeStep }) => ({ activeStep: activeStep + 1 }));
	handleBack = () => this.setState(({ activeStep }) => ({ activeStep: activeStep - 1 }));

	render() {
		const { classes } = this.props;
		const { order, activeStep } = this.state;

		const StageComponent = stepComponents[activeStep];

		return (
			<Paper className={classes.orderPaper}>
				<Typography component="h1" variant="h4" align="center">
					New order
				</Typography>
				<Stepper activeStep={activeStep}>
					{
						steps.map(step => (
							<Step key={step}>
								<StepLabel>{step}</StepLabel>
							</Step>
						))
					}
				</Stepper>
				<StageComponent
					order={order}
					setOrderField={this.setOrderField}
					resetOrder={this.resetOrder}
				/>
				<div className={classes.buttons}>
					{activeStep !== 0 && (
						<Button onClick={this.handleBack} className={classes.button}>
							Back
						</Button>
					)}
					<Button
						variant="contained"
						color="primary"
						onClick={this.handleNext}
						className={classes.button}
					>
						{activeStep === steps.length - 1 ? 'Book ticket' : 'Next'}
					</Button>
				</div>
			</Paper>
		);
	}
}

export default withStyles(styles)(NewOrder)
