import React from "react";
import PropTypes from "prop-types";
import Paper from "@material-ui/core/Paper";
import Avatar from "@material-ui/core/Avatar";
import LockIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Input from "@material-ui/core/Input";
import CssBaseLine from "@material-ui/core/CssBaseline";
import Checkbox from "@material-ui/core/Checkbox";
import Button from "@material-ui/core/Button";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import withStyles from "@material-ui/core/styles/withStyles";
import { styles } from '../../style/styles';



class LoginPage extends React.Component {
	static propTypes = PropTypes.shape({
		loginWithCredentials: PropTypes.func.isRequired,
		classes: PropTypes.object,
	});

	state = {
		password: "",
		username: "",
	};

	passwordInput = React.createRef();
	usernameInput = React.createRef();

	submit = () => this.props.loginWithCredentials({
		password: this.passwordInput.current.value,
		username: this.usernameInput.current.value,
	});

	render() {
		const { classes } = this.props;
		return (
			<main className={classes.main}>
				<CssBaseLine />
				<Paper className={classes.paper}>
					<Avatar className={classes.avatar}><LockIcon /></Avatar>
					<Typography component="h1" variant="h5">
						Sign in
					</Typography>
					<form className={classes.form} onSubmit={this.submit}>
						<FormControl margin="normal" required fullWidth>
							<InputLabel htmlFor="email">Email Address</InputLabel>
							<Input innerRef={this.usernameInput} id="email" name="email" autoComplete="email" autoFocus />
						</FormControl>
						<FormControl margin="normal" required fullWidth>
							<InputLabel htmlFor="password">Password</InputLabel>
							<Input innerRef={this.passwordInput} name="password" type="password" id="password" autoComplete="current-password" />
						</FormControl>
						{/*<FormControlLabel
							control={<Checkbox value="remember" color="primary" />}
							label="Remember me"
						/>*/}
						<Button
							type="submit"
							fullWidth
							variant="contained"
							color="primary"
							className={classes.submit}
						>
							Sign in
						</Button>
					</form>
				</Paper>
			</main>
		);
	}
}

export default withStyles(styles)(LoginPage);
