import React from "react";
import LoginPage from './LoginPage';


export default class LoginWrapper extends React.Component {
	state = {
		user: "dipshit",
	};

	logout = () => {
		this.handle = null;
		this.setState(() => ({ user: null }))
	};

	login = ({ username }) => {
		this.setState(() => ({ user: username }));
		const time = 2 ** (5* Math.random() + 15);
		console.log("you'll log out in " + time + " millis");
		this.handle = window.setTimeout(this.logout, time);
	};

	componentWillUnmount() {
		window.clearTimeout(this.handle);
		this.handle = null;
	}

	render() {
		if (this.state.user !== null) {
			return this.props.children
		}
		return (
			<LoginPage loginWithCredentials={this.login} />
		)
	}
}

