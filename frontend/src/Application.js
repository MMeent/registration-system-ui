import React from "react";
import { BrowserRouter, Redirect, Route, Switch } from "react-router-dom";
import MyPage from "./pages/my/MyPage";
import LoginWrapper from "./pages/auth/LoginWrapper";
import NavigationWrapper from './elements/NavigationWrapper';
import { hot } from "react-hot-loader";
import MyOrders from './pages/my/orders/MyOrders';
import NewOrder from './pages/my/orders/NewOrder';

class App extends React.Component {
	render() {
		return (
			<LoginWrapper>
				<BrowserRouter>
					<NavigationWrapper>
						<Switch>
							<Route path="/new-order" component={NewOrder} />
							<Route path="/my-orders" component={MyOrders} />
						</Switch>
					</NavigationWrapper>
				</BrowserRouter>
			</LoginWrapper>
		);
	}
}


export const Application = hot(module)(App);
export default Application