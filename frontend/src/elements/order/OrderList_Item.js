import React from "react";
import PropTypes from "prop-types";

import Collapse from "@material-ui/core/Collapse";
import Divider from "@material-ui/core/Divider";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Typography from "@material-ui/core/Typography";
import withStyles from "@material-ui/core/styles/withStyles";

import ExpandLessIcon from "@material-ui/icons/ExpandLess";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";

import { styles } from '../../style/styles';


const fmtOption = ({ name, cost }) => `${name} (€ ${cost.toFixed(2)})`;
const dayCost = ({ cost, extras }) => (cost + extras.reduce((tot, opt) => tot + opt.cost, 0));

class OrderList_Item extends React.Component {
	static propTypes = {
		order: PropTypes.shape({
			name: PropTypes.string.isRequired,
			items: PropTypes.arrayOf(PropTypes.shape({
				day: PropTypes.string.isRequired,
				extras: PropTypes.arrayOf(PropTypes.shape({
					name: PropTypes.string.isRequired,
					cost: PropTypes.number.isRequired,
				})).isRequired,
				cost: PropTypes.number.isRequired,
			})),
			extras: PropTypes.arrayOf(PropTypes.shape({
				name: PropTypes.string.isRequired,
				cost: PropTypes.number.isRequired,
			})),
		})
	};

	state = {
		open: false,
	};

	toggle = () => this.setState(({ open }) => ({ open: !open }));

	render() {
		const { classes, order } = this.props;
		const { open } = this.state;
		const subtext = `${order.items.length} days${ order.items.some(item => item.extras.length > 0) ? ", with extra options" : ""}`;

		return (
			<>
				<ListItem button onClick={this.toggle} >
					<ListItemText primary={ order.name } secondary={subtext} />
					<Typography variant="subtitle1" className={classes.total}>
						€ {(order.items.reduce((sum, item) => (sum + dayCost(item)), 0) + order.extras.reduce((sum, extra) => (sum + extra.cost), 0)).toFixed(2)}
					</Typography>
					{ open ? <ExpandLessIcon /> : <ExpandMoreIcon /> }
				</ListItem>
				<Collapse in={ open } timeout="auto">
					<List component="div" disablePadding>
						{
							order.items.map(item => (
								<ListItem button className={classes.nested}>
									<ListItemText
										inset
										primary={item.name}
										secondary={`Ticket (€ ${item.cost.toFixed(2)})` + (item.extras.reduce(
											(result, el) => (result === null)
												? ` + ${fmtOption(el)}`
												: `${result}, ${fmtOption(el)}`,
											null
										) || '')}
									/>
									<Typography variant="subtitle1" className={classes.total}>
										€ {dayCost(item).toFixed(2)}
									</Typography>
								</ListItem>
							))
						}
					</List>
				</Collapse>
			</>
		);
	}
}

export default withStyles(styles)(OrderList_Item)
